/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    fontFamily: {
      sans: ['Inter', 'sans-serif'],
    },
    color: {
      grey: '#E2E8F0',
      blue: '#0EA5E9',
    },
    extend: {},
  },
  plugins: [],
};
