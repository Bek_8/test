import { useEffect, useState } from 'react';
import AddComponent from './components/Add';
import Table from './components/Table';
import editIcon from './assets/edit.svg';
import deleteIcon from './assets/delete.svg';
import { DataTypes } from './utils/types';

const App = () => {
  const [data, setData] = useState<DataTypes[]>([]);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [id, setId] = useState<string>('');

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const table = window.localStorage.getItem('table');
      setData(table && JSON.parse(table));
    }
  }, []);

  const onEdit = (id: string) => {
    setIsEdit(true);
    setId(id);
  };

  const onDelete = (id: string) => {
    if (typeof window !== 'undefined') {
      const table = window.localStorage.getItem('table');
      const parsed = table && JSON.parse(table);
      const deletedItem = parsed.filter((item: DataTypes) => item.id !== id);
      window.localStorage.setItem('table', JSON.stringify(deletedItem));
      window.location.reload();
    }
  };

  const columns = [
    { key: 'name', name: 'Name' },
    { key: 'age', name: 'Age' },
    { key: 'status', name: 'Status' },
    { key: 'employment', name: 'Employment' },
    { key: 'action', name: 'Action' },
  ];

  const rows = data?.map((item) => {
    return {
      name: item.name,
      status: (
        <div
          className={`h-6 flex items-center ${
            item.status === 'subscribed' ? 'bg-blue-500' : 'bg-slate-300'
          }`}
        >
          {item.status === 'subscribed' ? item.status : 'Not Subscribed'}
        </div>
      ),
      employment: (
        <div
          className={`flex items-center h-6 ${
            item.employed === 'employed' ? 'bg-blue-500' : 'bg-slate-300'
          }`}
        >
          {item.employed === 'employed' ? item.employed : 'Unemployed'}
        </div>
      ),
      age: item.age,
      action: (
        <div className="flex justify-between">
          <div className="mx-2 cursor-pointer	" onClick={() => onEdit(item.id)}>
            <img src={editIcon} alt="edit" />
          </div>
          <div className="mx-2 cursor-pointer	" onClick={() => onDelete(item.id)}>
            <img src={deleteIcon} alt="delete" />
          </div>
        </div>
      ),
    };
  });
  return (
    <div className="flex w-screen">
      <AddComponent isEdit={isEdit} id={id} />
      <Table column={columns} row={rows ? rows : []} />
    </div>
  );
};

export default App;
