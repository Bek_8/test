interface Props {
  title: string;
  onChange: (value: string) => void;
  placeholder?: string;
  type?: string;
}

const Input = ({ title, onChange, placeholder = '', type = '' }: Props) => {
  return (
    <div>
      <div className="text-base font-medium mt-6">{title}</div>
      <input
        type={type}
        placeholder={placeholder}
        onChange={(e) => onChange(e.target.value)}
        className="border-solid border-grey border-2 rounded mt-2.5 w-full"
      />
    </div>
  );
};

export default Input;
