import DataGrid from 'react-data-grid';
import 'react-data-grid/lib/styles.css';

interface Props<T> {
  column: {
    key: string;
    name: string;
  }[];
  row: T[];
}

const CustomTable = <T,>({ column, row }: Props<T>) => {
  return (
    <div className="w-4/6 p-10">
      <div className="text-xl font-bold mb-4">Customers</div>
      <DataGrid
        columns={column}
        className={'dataGrid rdg-light'}
        rows={row}
        defaultColumnOptions={{
          sortable: true,
        }}
      />
    </div>
  );
};

export default CustomTable;
