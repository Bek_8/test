import { useState } from 'react';
import { DataTypes } from '../../utils/types';
import Button from '../Button';
import Input from '../Input';

const uid = function () {
  return Date.now().toString(36) + Math.random().toString(36).substr(2);
};

interface Props {
  isEdit: boolean;
  id: string;
}

const AddComponent = ({ id, isEdit }: Props) => {
  const [name, setName] = useState<string>('');
  const [age, setAge] = useState<string>('');
  const [status, setStatus] = useState<string>('notsubscribed');
  const [employed, setEmployed] = useState<string>('unemployed');

  const onSubmit = () => {
    const body = {
      name,
      age,
      status,
      employed,
      id: uid(),
    };

    if (typeof window !== 'undefined') {
      const table = window.localStorage.getItem('table');
      const parsed = table && JSON.parse(table);

      if (table?.length) {
        if (isEdit) {
          const editingData = parsed?.map((item: DataTypes) => {
            if (item.id === id) {
              const body = {
                name: name.length ? name : item.name,
                age: age.length ? age : item.age,
                status: status !== item.status ? status : item.status,
                employed: employed !== item.employed ? employed : item.employed,
                id: uid(),
              };
              return body;
            }
            return item;
          });
          window.localStorage.setItem('table', JSON.stringify(editingData));
          window.location.reload();
        } else {
          parsed.push(body);
          window.localStorage.setItem('table', JSON.stringify(parsed));
          window.location.reload();
        }
      } else {
        window.localStorage.setItem('table', JSON.stringify([body]));
        window.location.reload();
      }
    }
  };

  const getEditingData = () => {
    if (typeof window !== 'undefined') {
      const table = window.localStorage.getItem('table');
      const parsed = table && JSON.parse(table);
      return parsed?.filter((item: DataTypes) => {
        return item.id === id;
      });
    }
  };

  return (
    <div className="w-2/6 p-10">
      <div className="text-xl font-bold mb-4">{isEdit ? 'Edit' : 'Add'} Customer</div>
      <div className="flex justify-between align-center">
        <Input
          onChange={(e) => setName(e)}
          title="First Name"
          placeholder={isEdit ? getEditingData()[0]?.name : ''}
        />
        <Input
          onChange={(e) => setAge(e)}
          title="Age"
          type="number"
          placeholder={isEdit ? getEditingData()[0]?.age : ''}
        />
      </div>
      <div className="text-base font-medium mt-6">Status</div>
      <div className="w-full bg-slate-300 p-2 mt-2.5 rounded flex justify-between">
        <button
          onClick={() => setStatus('subscribed')}
          className={`flex justify-center w-1/2 p-1 ${
            status === 'subscribed' ? 'bg-white rounded' : ''
          }`}
        >
          Subscribed
        </button>
        <button
          onClick={() => setStatus('notsubscribed')}
          className={`flex justify-center w-1/2 p-1 ${
            status === 'notsubscribed' ? 'bg-white rounded' : ''
          }`}
        >
          Not Subscribed
        </button>
      </div>
      <div className="text-base font-medium mt-6">Employment</div>
      <div className="w-full bg-slate-300 p-2 mt-2.5 rounded flex justify-between">
        <button
          onClick={() => setEmployed('employed')}
          className={`flex justify-center w-1/2 p-1 ${
            employed === 'employed' ? 'bg-white rounded' : ''
          }`}
        >
          Employed
        </button>
        <button
          onClick={() => setEmployed('unemployed')}
          className={`flex justify-center w-1/2 p-1 ${
            employed === 'unemployed' ? 'bg-white rounded' : ''
          }`}
        >
          Unemployed
        </button>
      </div>

      <Button onClick={onSubmit} label={isEdit ? 'Edit' : 'Save'} />
    </div>
  );
};

export default AddComponent;
