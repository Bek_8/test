interface Props {
  label: string;
  onClick: () => void;
}

const Button = ({ label, onClick }: Props) => {
  return (
    <div>
      <button
        onClick={onClick}
        className="rounded p-3 text-center bg-blue-500 my-6 text-white	w-full"
      >
        {label}
      </button>
    </div>
  );
};

export default Button;
