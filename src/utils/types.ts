export interface DataTypes {
  name: string;
  age: string;
  status: string;
  employed: string;
  id: string;
}
